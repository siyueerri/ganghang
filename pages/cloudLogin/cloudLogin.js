Page({
  data: {
    loginBtnState: true,
    userInfo: {},
    userN: '',
    passW: ''
  },
  userNameInput: function (e) {
    // this.setData({
    //   userN: e.detail.value
    // })
    console.log(e);
    var val = e.detail.value;
    if (val != '') {
      this.setData({
        userN: e.detail.value
      });
      if (this.data.passW != "") {
        this.setData({
          loginBtnState: false
        })
      }
    }
    else {
      this.setData({
        // 需要加下面这句
        userN: "",
        loginBtnState: true
      });
    }
  },
  passWordInput: function (e) {
    // this.setData({
    //   passW: e.detail.value
    // })
    console.log(e);
    var val = e.detail.value;
    if (val != '') {
      this.setData({
        passW: e.detail.value
      });
      if (this.data.userN != "") {
        this.setData({
          loginBtnState: false
        })
      }
    }
    else {
      this.setData({
        // 需要加下面这句
        passW: "",
        loginBtnState: true
      });
    }
  },
  loginBtnClick: function (a) {
    var that = this
    if (that.data.userN.length == 0 || that.data.passW.length == 0) {
      wx.showModal({
        title: '温馨提示：',
        content: '用户名或密码不能为空！',
        showCancel: false
      })
    } else {
      wx.cloud.database().collection('users').where({ username: that.data.userN }).get({
        success(res) {
          if (res.data[0] == null) {
            wx.showModal({
              title: '用户名错误',
              content: '用户名错误'//session中用户名和密码不为空触发
            });
          }
          else {  //密码正确
            if (that.data.passW == res.data[0].password) {
              try {
                // 同步接口立即写入
                wx.setStorageSync('localusername', res.data[0].username)
                console.log('写入成功')
              } catch (e) {
                console.log('写入发生错误')
              }
              wx.switchTab({
                url: '/pages/tasks/tasks'//[主页面]
              })
            }
            else {
              wx.showModal({
                title: '密码错误',
                content: '密码错误'//session中用户名和密码不为空触发
              });
            }
          }
        }

      })
    }
  },
  signBtnClick: function (a) {
    wx.switchTab({
      url: '/pages/sign/sign'//[主页面]
    })
  },

})