// index.js
const db = wx.cloud.database()

Page({
  data: {
    localusername:'',
    tasks: [],
    isClick: 0,
    navbar: ['未接任务', '我的揭榜', '我的发布'],
    currentTab: 0
  },

  onLoad: function (options) {
    try {
      // 同步接口立即返回值
      var localusername = wx.getStorageSync('localusername')
      console.log(localusername)
      this.setData({
        localusername:localusername
      })
    } catch (e) {
      console.log('读取发生错误')
    }

    db.collection("tasks").where({
      isAccept: 0
    }).get({
      success: res => {
        this.setData({
          tasks: res.data,
        })
        console.log(res.data)
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '加载任务失败',
        })
      }
    })
  },

  onPostTap: function (event) {
    // 获取本页面的id
    var taskId = event.currentTarget.dataset.taskid;
    console.log(taskId)
    var app = getApp();
    app.requestDetailid = taskId;
    wx.navigateTo({
      //将本页面的id传到需要跳转的页面
      url: "../videoDetail/videoDetail?id=" + taskId
    })
  },

  acceptTask: function (event) {
    var taskId = event.currentTarget.dataset.taskid;
    console.log(taskId)
    var localusername = this.data.localusername
    db.collection("tasks").where({
      _id: taskId
    }).update({
      data: {
        isAccept: 1,
        accepter: localusername,
      },
    }).then(res => {
      console.log(res)
      wx.showToast({
        icon: "none",
        title: '揭榜成功',
      })
    })
  },
  //未接任务
  navbarTap0: function (e) {
    db.collection("tasks").where({
      isAccept: 0
    }).get({
      success: res => {
        this.setData({
          tasks: res.data,
        })
        console.log(res.data)
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '加载失败',
        })
      }
    })
    this.setData({
      currentTab: 0
    })
  },
  //我的揭榜
  navbarTap1: function (e) {
    db.collection("tasks").where({
      accepter: this.data.localusername,
      isAccept: 1,
      isAchieve: 0,
    }).get({
      success: res => {
        this.setData({
          tasks: res.data,
        })
        console.log(res.data)
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '加载失败',
        })
      }
    })
    this.setData({
      currentTab: 1
    })
  },
  //已完成
  navbarTap2: function (e) {
    db.collection("tasks").where({
      accepter: this.data.localusername,
      isAccept: 1,
      isAchieve: 1,
    }).get({
      success: res => {
        this.setData({
          tasks: res.data,
        })
        console.log(res.data)
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '加载失败',
        })
      }
    })
    this.setData({
      currentTab: 2
    })
  }
})
