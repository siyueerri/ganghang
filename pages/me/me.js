// pages/me/me.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    localusername: '',
  },

  exitLogin: function (e) {
    wx.switchTab({
      url: '/pages/cloudLogin/cloudLogin'//[主页面]
    })
  },
  goToFabu: function (e) {
    wx.switchTab({
      url: '/pages/myPublish/myPublish'//[主页面]
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    try {
      // 同步接口立即返回值
      var localusername = wx.getStorageSync('localusername')
      console.log(localusername)
      this.setData({
        localusername: localusername
      })
    } catch (e) {
      console.log('读取发生错误')
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})