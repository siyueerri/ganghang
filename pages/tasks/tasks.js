// index.js
const db = wx.cloud.database()

Page({
  data: {
    localusername:'',
    numOfjinxing:0,
    numOfwanjie:0,
    numOfleiji:0,
  },

  onLoad: function (options) {
    try {
      // 同步接口立即返回值
      var localusername = wx.getStorageSync('localusername')
      console.log(localusername)
      this.setData({
        localusername:localusername
      })
    } catch (e) {
      console.log('读取发生错误')
    }

    db.collection("tasks").where({
      accepter: this.data.localusername,
      isAccept: 1,
      isAchieve: 0,
    }).get({
      success: res => {
        this.setData({
          numOfjinxing: res.data.length,
        })
        console.log(this.data.numOfjinxing)
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '加载失败',
        })
      }
    })

    db.collection("tasks").where({
      accepter: this.data.localusername,
      isAccept: 1,
      isAchieve: 1,
    }).get({
      success: res => {
        this.setData({
          numOfwanjie: res.data.length,
        })
        console.log(this.data.numOfwanjie)
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '加载失败',
        })
      }
    })
  },
})

