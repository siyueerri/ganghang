Page({

  data: {
      userInfo: '', //用于存放获取的用户信息
  },
  onLoad(){
      let user = wx.getStorageSync('user')
      this.setData({
        userInfo: user
      })
  },
  // 授权登录
  login() {
      wx.getUserProfile({
          desc: '必须授权才能继续使用', // 必填 声明获取用户个人信息后的用途，后续会展示在弹窗中
          success:()=> { 
              wx.showToast({
                title: '登陆成功',
                duration:2000,
                success:function(){
                  wx.navigateTo({
                    url: '../index/index',
                  })
                }
              })
          },
          fail:(err)=> {
              console.log('授权失败', err);
          }
      })
  },
  // 退出登录
  loginOut(){
      this.setData({ 
          userInfo:''
      })
      // 清空缓存
      wx.setStorageSync('user',null)
  }
  
})
